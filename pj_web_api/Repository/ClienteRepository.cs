﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using pj_web_api.Infra;
using pj_web_api.Model;
using Microsoft.Extensions.Configuration;
using System.Data;

namespace pj_web_api.Repository
{
    public class ClienteRepository : BaseRepository<DefaultConnection>, IClienteRepository
    {
        public ClienteRepository(IConfiguration configuration) : base(configuration) { }

        public IEnumerable<Cliente> GetAll()
        {
            using (IDbConnection connection = Connection)
            {
                string query = "SELECT * FROM Cliente";
                return connection.Query<Cliente>(query);
            }
        }

        public bool Delete(int id)
        {
            using (IDbConnection connection = Connection)
            {
                string query = "DELETE FROM Cliente WHERE Id = @id";
                var queryResult = connection.Execute(query, new { id });
                if (queryResult > 0)
                    return true;
                else
                    return false;
            }
        }

        public Cliente GetClienteById(int id)
        {
            using (IDbConnection connection = Connection)
            {
                string query = "SELECT * FROM Cliente WHERE Id = @id";
                return connection.QueryFirstOrDefault<Cliente>(query, new { id });
            }
        }

        public bool SaveOrUpdate(Cliente cliente)
        {
            using (IDbConnection connection = Connection)
            {
                if (cliente.Id > 0)
                {
                    string queryUpdate = "UPDATE Cliente SET (Name = @Name, Email = @Email, ConsultorId = @IdConsultor WHERE Id = @Id)";
                    var queryResultUpdate = connection.Execute(queryUpdate, new
                    {
                        cliente.Name,
                        cliente.Email,
                        cliente.IdConsultor,
                        cliente.Id
                    });
                    if (queryResultUpdate > 0)
                        return true;
                    else
                        return false;

                }
                string queryInsert = "INSERT INTO Cliente (Name, Email, IdConsultor) VALUES (@Name, @Email, @IdConsultor)";
                var queryResultInsert = connection.Execute(queryInsert, new
                {
                    cliente.Name,
                    cliente.Email,
                    cliente.IdConsultor
                });
                if (queryResultInsert > 0)
                    return true;
                else
                    return false;
            }
        }
    }
}
