﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;

namespace pj_web_api.Infra
{
    public class BaseRepository<TConnectionString> where TConnectionString : IConnectionString, new()
    {
        private readonly IConnectionString connectionString;
        private readonly IConfiguration configuration;

        public BaseRepository(IConfiguration configuration)
        {
            this.configuration = configuration;
            this.connectionString = new TConnectionString();
        }

        public IDbConnection Connection
        {
            get
            {
                var con = new SqlConnection(configuration.GetConnectionString(connectionString.Name));
                con.Open();
                return con;
            }
        }

    }
}
