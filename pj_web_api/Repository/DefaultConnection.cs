﻿using pj_web_api.Infra;

namespace pj_web_api.Repository
{
    public class DefaultConnection : IConnectionString
    {
        public string Name => "DefaultConnection";
    }
}