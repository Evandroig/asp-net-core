﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace pj_web_api.Model
{
    public class TodoItem
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool isComplete { get; set; }
    }
}
