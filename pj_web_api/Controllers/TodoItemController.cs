﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using pj_web_api.Application;
using pj_web_api.Model;

namespace pj_web_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TodoItemController : ControllerBase
    {
        private readonly ITodoItemService todoItemService;

        public TodoItemController(ITodoItemService todoItemService)
        {
            this.todoItemService = todoItemService;
        }

        [HttpGet]
        [Route("/api/todoitem/")]
        public dynamic Get()
        {
            return todoItemService.GetAll();
        }

        [HttpGet]
        [Route("/api/todoitem/{id}")]
        public dynamic Get([FromRoute]int id)
        {
            var todoItem = todoItemService.GetById(id);
            if (todoItem != null)
                return Ok(new
                {
                    Status = "Sucess",
                    Content = todoItem
                });
            else
                return Ok(new
                {
                    Status = "Sucess",
                    Content = "Não há tarefas cadasatradas"
                });

        }

        [HttpDelete]
        [Route("/api/todoitem/{id}")]
        public dynamic Delete([FromRoute]int id)
        {
            if (todoItemService.Delet(id))
                return Ok(new
                {
                    Status = "Sucess",
                    Content = "Tarefa excluida com sucesso"
                });
            else
                return Ok(new
                {
                    Status = "Error",
                    Content = "Tarefa inválida"
                });
        }

        [HttpPut]
        [Route("/api/todoitem")]
        public dynamic Put([FromBody] TodoItem todoItem)
        {
            if (todoItemService.SaveOrupdate(todoItem))
                return Ok(new
                {
                    Status = "Sucess",
                    Content = "Tarefa adicionada com sucesso"
                });
            else
                return Ok(new
                {
                    Status = "Error",
                    Content = "Falha ao tentar adicionar a tarefa"
                });
        }
        [HttpGet("Check")]
        [Route("/api/todoitem/check/{id}")]
        public dynamic Check([FromRoute]int id)
        {
            if (todoItemService.Check(id))
                return Ok(new
                {
                    Status = "Sucess",
                    Content = "Tarefa concluída"
                });
            else
                return Ok(new
                {
                    Status = "Error",
                    Content = "Falha ao tentar concluir a tarefa"
                });
        }

        [HttpGet("UnCheck")]
        [Route("/api/todoitem/Uncheck/{id}")]
        public dynamic UnCheck([FromRoute]int id)
        {
            if (todoItemService.Check(id))
                return Ok(new
                {
                    Status = "Sucess",
                    Content = "A tarefa foi marcada como não concluída"
                });
            else
                return Ok(new
                {
                    Status = "Error",
                    Content = "Falha ao tentar alterar o status da tarefa"
                });
        }
    }
}