﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace PJ_WEB.Controllers
{
    public class ClienteController : Controller
    {
        [Route("/Cliente")]
        public IActionResult Index()
        {
            return View("Cliente");
        }
    }
}