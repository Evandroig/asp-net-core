﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace pj_web_api.Infra
{
    public interface IConnectionString
    {
        string Name { get;}
    }
}
