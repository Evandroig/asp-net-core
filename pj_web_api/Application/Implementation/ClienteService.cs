﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using pj_web_api.Model;
using pj_web_api.Repository;

namespace pj_web_api.Application.Implementation
{
    public class ClienteService : IClienteService
    {
        private readonly IClienteRepository clienteRepository;

        public ClienteService(IClienteRepository clienteRepository)
        {
            this.clienteRepository = clienteRepository;
        }

        public IEnumerable<Cliente> GetAll()
        {
            return clienteRepository.GetAll();
        }
        public bool Delete(int id)
        {
            return clienteRepository.Delete(id);
        }

        public Cliente GetClienteById(int id)
        {
            return clienteRepository.GetClienteById(id);
        }

        public bool SaveOrUpdate(Cliente cliente)
        {
            return clienteRepository.SaveOrUpdate(cliente);
        }
    }
}
