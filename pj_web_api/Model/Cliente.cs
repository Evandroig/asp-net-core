﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace pj_web_api.Model
{
    public class Cliente
    {
        public int Id { get; set; }
        public string Name {get; set;}
        public string Email { get; set; }
        public int IdConsultor { get; set; }
    }
}
