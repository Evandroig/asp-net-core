﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using pj_web_api.Model;

namespace pj_web_api.Repository
{
    public interface IClienteRepository
    {
        IEnumerable<Cliente> GetAll();
        Cliente GetClienteById(int id);

        bool SaveOrUpdate(Cliente cliente);

        bool Delete(int id);
    }
}
