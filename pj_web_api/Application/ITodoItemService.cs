﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using pj_web_api.Model;

namespace pj_web_api.Application
{
    public interface ITodoItemService
    {
        bool SaveOrupdate(TodoItem todoItem);
        bool Delet(int id);
        bool Check(int id);
        bool UnCheck(int id);
        IEnumerable<TodoItem> GetAll();
        TodoItem GetById(int id);
    }
}
