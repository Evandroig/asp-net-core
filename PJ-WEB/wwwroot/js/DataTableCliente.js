﻿$(document).ready(() => {
    $('table').DataTable({
        responsive: true,
        ajax: {
            url: "https://localhost:44342/api/cliente",
            method: "GET",
            dataType: "json",
            crossDomain: true,
            header : "Access-Control-Allow-Origin: https://localhost:44342",
            dataSrc: "",
        },

        columns: [
            { data: "id" },
            { data: "name" },
            { data: "email" },
            { data: "idConsultor" },
            {
                targets: [5],
                data: "id",
                render: function (data) {
                    return '<a class="btn btn-secondary" id="'+data+'" href="#"target_blank>Excluir</a>'
                }
            }
     
        ],

        aoColumnDefs: [{ "bVisible": false, "aTargets": [0] }],

        language: {
            loadingRecords: "Carregando Dados...",
            search: "Pesquisar",
            info: "Exibindo _START_ de _END_ of _TOTAL_ resultados.",
            paginate: {
                previous: "Voltar",
                next: "Avançar",
            },
            lengthMenu: "Exibir _MENU_",
        },

        "order": [[1, 'asc']]
    });
});