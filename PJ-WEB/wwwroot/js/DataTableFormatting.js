﻿function formatoDataTable() {
    $('#example1').DataTable({
        "language": {
            "sEmptyTable": "Nenhum registro encontrado",
            "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
            "sInfoFiltered": "(Filtrados de _MAX_ registros)",
            "sInfoPostFix": "",
            "sInfoThousands": ".",
            "sLengthMenu": "Resultados por página _MENU_",
            "sLoadingRecords": "Carregando...",
            "sProcessing": "Processando...",
            "sZeroRecords": "Nenhum registro encontrado",
            "sSearch": "Pesquisar: ",
            "oPaginate": {
                "sNext": "Próximo",
                "sPrevious": "Anterior",
                "sFirst": "Primeiro",
                "sLast": "Último"
            },
            "oAria": {
                "sSortAscending": ": Ordenar colunas de forma ascendente",
                "sSortDescending": ": Ordenar colunas de forma descendente"
            }
        },
        select: true
    })
}

function formatoDataTableExport() {


    var table = $('#example1').DataTable({
        "language": {
            "sEmptyTable": "Nenhum registro encontrado",
            "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
            "sInfoFiltered": "(Filtrados de _MAX_ registros)",
            "sInfoPostFix": "",
            "sInfoThousands": ".",
            "sLengthMenu": "Resultados por página _MENU_",
            "sLoadingRecords": "Carregando...",
            "sProcessing": "Processando...",
            "sZeroRecords": "Nenhum registro encontrado",
            "sSearch": "Pesquisar: ",
            "oPaginate": {
                "sNext": "Próximo",
                "sPrevious": "Anterior",
                "sFirst": "Primeiro",
                "sLast": "Último"
            },
            "oAria": {
                "sSortAscending": ": Ordenar colunas de forma ascendente",
                "sSortDescending": ": Ordenar colunas de forma descendente"
            },

            "buttons": {
                colvis: 'Colunas Visíveis'
            }
        },
        "paging": false,
        lengthChange: false,
        buttons: ['excel', 'pdf', 'colvis']
    });

    table.buttons().container()
        .appendTo('#example1_wrapper .col-md-6:eq(0)');

    var botes = $(".btn-secondary");

    botes.removeClass("btn-secondary");
    botes.addClass("btn-danger");

    $('#example1').parent().addClass("overflow-x-auto");
}

$(document).ready(function () {

    var parametro = window.location.pathname.split("/");
    parametro = parametro[2].toUpperCase();

    if (parametro.toUpperCase() == "PRODUTO" ||
        parametro.toUpperCase() == "CLIENTE")
        formatoDataTableExport();
    else
        formatoDataTable();
});