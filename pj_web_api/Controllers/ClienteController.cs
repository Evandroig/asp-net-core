﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using pj_web_api.Application;
using pj_web_api.Model;
using System.Web.Script.Serialization;
using Microsoft.AspNetCore.Cors;

namespace pj_web_api.Controllers
{
    [EnableCors("MyPolicy")]
    [Route("api/[controller]")]
    [ApiController]
    public class ClienteController : ControllerBase
    {
        private readonly IClienteService clienteService;

        public ClienteController(IClienteService clienteService)
        {
            this.clienteService = clienteService;
        }
        [HttpGet]
        public dynamic Get()
        {
           return clienteService.GetAll();
           
        }
        [HttpPost]
        public dynamic Put(Cliente cliente)
        {
            if (clienteService.SaveOrUpdate(cliente))
                return Ok(new
                {
                    Status = "Sucess",
                    Content = "Usuário adicionado com sucesso"
                });
            else
                return Ok(new
                {
                    Status = "Error",
                    Content = "Falha ao tentar adicionar o cliente"
                });
        }
        [HttpGet]
        [Route("/api/cliente/{id}")]
        public dynamic Get([FromRoute]int id)
        {
            var cliente = clienteService.GetClienteById(id);
            if (cliente != null)
                return Ok(new
                {
                    Status = "Sucess",
                    Content = cliente
                });
            else
                return Ok(new
                {
                    Status = "Error",
                    Content = "Cliente não encontrado"
                });
        }
        [HttpDelete]
        [Route("/api/cliente/{id}")]
        public dynamic Delet(int id)
        {
            if (clienteService.Delete(id))
                return Ok(new
                {
                    Status = "Sucess",
                    Content = "Cliente excluido com sucesso"
                });
            else
                return Ok(new
                {
                    Status = "Error",
                    Content = "Cliente não encontrado"
                });
        }
    }
}