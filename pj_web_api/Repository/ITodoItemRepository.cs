﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using pj_web_api.Model;

namespace pj_web_api.Repository
{
    public interface ITodoItemRepository
    {
        bool SaveOrUpdate(TodoItem todoItem);
        bool Delet(int id);
        bool Check(int id);
        bool UnChek(int id);
        IEnumerable<TodoItem> GetAll();
        TodoItem GetById(int id);
    }
}
