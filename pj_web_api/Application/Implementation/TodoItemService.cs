﻿using pj_web_api.Model;
using pj_web_api.Repository;
using System.Collections.Generic;

namespace pj_web_api.Application.Implementation
{
    public class TodoItemService : ITodoItemService
    {
        private readonly ITodoItemRepository todoItemRepository;
        public TodoItemService(ITodoItemRepository todoItemRepository)
        {
            this.todoItemRepository = todoItemRepository;
        }
        public bool Check(int id)
        {
            return todoItemRepository.Check(id);
        }

        public bool Delet(int id)
        {
            return todoItemRepository.Delet(id);
        }

        public bool SaveOrupdate(TodoItem todoItem)
        {
            return todoItemRepository.SaveOrUpdate(todoItem);
        }

        public bool UnCheck(int id)
        {
            return todoItemRepository.UnChek(id);
        }

        public IEnumerable<TodoItem> GetAll()
        {
            return todoItemRepository.GetAll();
        }

        public TodoItem GetById(int id)
        {
            return todoItemRepository.GetById(id);
        }

    }
}
