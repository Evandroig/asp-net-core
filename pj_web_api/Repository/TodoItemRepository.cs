﻿using Microsoft.Extensions.Configuration;
using pj_web_api.Infra;
using pj_web_api.Model;
using System;
using System.Collections.Generic;
using System.Data;
using Dapper;
using System.Linq;
using System.Threading.Tasks;

namespace pj_web_api.Repository
{
    public class TodoItemRepository : BaseRepository<DefaultConnection>, ITodoItemRepository
    {
        public TodoItemRepository(IConfiguration configuration) : base(configuration) { }

        public bool Check(int id)
        {
            using (IDbConnection connection = Connection)
            {
                string query = "UPDATE FROM TodoItem SET isComplete = true WHERE Id = @id";
                var queryResult = connection.Execute(query, new { id });
                if (queryResult > 0)
                    return true;
                else
                    return false;
            }
        }

        public bool Delet(int id)
        {
            using (IDbConnection connection = Connection)
            {
                string query = "DELETE FROM TodoItem WHERE Id = @id";
                var queryResult = connection.Execute(query, new { id });
                if (queryResult > 0)
                    return true;
                else
                    return false;

            }
        }

        public IEnumerable<TodoItem> GetAll()
        {
            using (IDbConnection connection = Connection)
            {
                string query = "SELECT * FROM TodoItem";
                return connection.Query<TodoItem>(query);
            }
        }

        public TodoItem GetById(int id)
        {
            using (IDbConnection connection = Connection)
            {
                string query = "SELECT * FROM TodoItem WHERE Id = @id";
                return connection.QueryFirstOrDefault<TodoItem>(query, new {  id });
            }
        }

        public bool SaveOrUpdate(TodoItem todoItem)
        {
            using (IDbConnection connection = Connection)
            {
                if (todoItem.Id > 0)

                {
                    string queryUpdate = "UPDATE FROM TodoItem SET Name = @Name, IsComplete = @IsComplete WHERE Id = @id";
                    var queryResultUpdate = connection.Execute(queryUpdate, new { todoItem.Name, todoItem.isComplete, todoItem.Id });
                    if (queryResultUpdate > 0)
                        return true;
                    else
                        return false;
                }
                string queryInsert = "INSERT INTO TodoItem (Name, IsComplete) VALUES (@Name, @IsComplete)";
                var queResultInsert = connection.Execute(queryInsert, new { todoItem.Name, todoItem.isComplete });
                if (queResultInsert > 0)
                    return true;
                else
                    return false;
            }
        }

        public bool UnChek(int id)
        {
            using (IDbConnection connection = Connection)
            {
                string query = "UPDATE FROM TodoItem SET isComplete = false WHERE Id = @id";
                var queryResult = connection.Execute(query, new { id });
                if (queryResult > 0)
                    return true;
                else
                    return false;
            }

        }
    }
}
